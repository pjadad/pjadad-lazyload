var lazyLoad = (function($) {
  return function($items) {
    $items = $items ? $items : $('div[data-lazy-src]');
    
    $items.each(function() {

      var $item = $(this);

      if ($item.hasClass('loaded') || $item.hasClass('loading')) {
        return;
      }
      
      var watcher = scrollMonitor.create(this, 150);

      // add reveal event that only fires once
      watcher.one('enterViewport', function() {
        var $item = $(this.watchItem);

        $item.addClass('loading');

        // send of as anonymous function to handle multiple images
        (function($item) {
          var src = $item.data('lazy-src');

          // append child div with background image
          $item.append('<div style="background-image: url(' + src + ')" />');

          // reveal child div when image is loaded, via loaded class
          $('<img />')
            .on('load', function() {
              $item
                .addClass('loaded')
                .removeClass('loading');
            })
            .attr('src', src);
        })($item);
      });
    });
  };
})(jQuery);